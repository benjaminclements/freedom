
public abstract class ACar implements ICar{
	
	private String make;
	private String model;
	private int year;
	private int mileage;
	
	public ACar(String inMake, String inModel, int inYear) {
		// TODO Auto-generated constructor stub
		make = inMake;
		model = inModel;
		year = inYear;
	}
	
	protected void setMileage(int inMileage){
		mileage = inMileage;
	}
	
	@Override
	public String getMake(){
		return make;
	}
	
	public String getModel(){
		return model;
	}
	
	public int getYear(){
		return year;
	}
	
	public int getMileage(){
		return mileage;
	}

}
